NAME
    Apt::PM - locate Perl Modules in Debian repositories

SYNOPSIS
    Cmd-line:

            apt-pm update
            apt-pm find Moose
        
            # print out all dependencies of an unpacked distribution that are packaged for Debian
            perl -MApt::PM -MModule::Depends -le \
                    '$apm=Apt::PM->new();$md=Module::Depends->new->dist_dir(".")->find_modules; %r=(%{$md->requires},%{$md->build_requires}); while (($m, $v) = each %r) { $f=$apm->find($m, $v); print $f->{"min"}->{"package"} if $f->{"min"}  }' \
                    | sort \
                    | uniq \
                    | xargs echo apt-get install
            # print out all dependencies of an unpacked distribution that are not packaged for Debian
            perl -MApt::PM -MModule::Depends -le \
                    '$apm=Apt::PM->new();$md=Module::Depends->new->dist_dir(".")->find_modules; %r=(%{$md->requires},%{$md->build_requires}); while (($m, $v) = each %r) { $f=$apm->find($m, $v); print $m, " ", $v if not $f->{"min"}  }'

    Module:

            my $aptpm = Apt::PM->new(sources => [ 'PerlPackages.bz2' ])
            $aptpm->update;
            my %moose_locations = $aptpm->find('Moose');

USAGE
  COMMAND-LINE USAGE
    Add sources for Debian releases and components. Here is the complete
    list that can be reduced just to the wanted ones:

            cat >> /etc/apt/sources.list << __END__
            # for apt-pm
            deb http://pkg-perl.alioth.debian.org/~jozef-guest/pmindex/ etch    main contrib non-free
            deb http://pkg-perl.alioth.debian.org/~jozef-guest/pmindex/ lenny   main contrib non-free
            deb http://pkg-perl.alioth.debian.org/~jozef-guest/pmindex/ sid     main contrib non-free
            deb http://pkg-perl.alioth.debian.org/~jozef-guest/pmindex/ squeeze main contrib non-free
            __END__

    Fetch the indexes:

            apt-pm update

    Look for the CPAN modules:

            $ apt-pm find Moose::Meta::Attribute
            libmoose-perl_0.98-1_i386: Moose::Meta::Attribute 0.98
            libmoose-perl_0.54-1_all: Moose::Meta::Attribute 0.54
            libmoose-perl_0.17-1_all: Moose::Meta::Attribute 0.08

    Look for the non-CPAN modules:

            apt-pm find Purple        
            # libpurple0_2.4.3-4lenny5_i386: Purple 0.01
        
            apt-pm find Dpkg::Version
            # dpkg-dev_1.14.28_all: Dpkg::Version 0

METHODS
  new()
    Object constructor.

    PROPERTIES
    sources
        `isa => 'ArrayRef'' of files that will be read to construct the
        lookup. By default it is filled with files from
        /var/cache/apt/apt-pm/.

    cachedir
        Is the folder where indexes cache files will be stored. Default is
        /var/cache/apt/apt-pm/deb/.

    repo_type
        `deb|deb-src'

  find($module_name, [$min_version])
    Returns hash with Perl versions as key and hash value having Debian
    version and package name. Example:

            {
                    '0.94' => {
                            'version' => '0.94-1',
                            'package' => 'libmoose-perl'
                            'arch'    => 'i386'
                    },
                    '0.97' => {
                            'version' => '0.97-1',
                            'package' => 'libmoose-perl'
                            'arch'    => 'i386'
                    },
                    '0.54' => {
                            'version' => '0.54-1',
                            'package' => 'libmoose-perl'
                            'arch'    => 'i386'
                    },
            };

    If `$min_version' is set, returns `min' and `max' keys. `max' has always
    the highest version:

            'max' => {
                    'version' => '0.97-1',
                    'package' => 'libmoose-perl'
                    'arch'    => 'i386'
            },

    `min' is changing depending on `$min_version'. Examples:

            $min_version = '0.01';
            'min' => {
                    'version' => '0.54-1',
                    'package' => 'libmoose-perl'
                    'arch'    => 'i386'
            },
            $min_version = '0.93';
            'min' => {
                    'version' => '0.94-1',
                    'package' => 'libmoose-perl'
                    'arch'    => 'i386'
            },
            $min_version = '1.00';
            'min' => undef,

  update
    Scans the /etc/apt/sources.list and /etc/apt/sources.list.d/*.list
    repositories for PerlPackages.bz2 and prepares them to be used for find.
    All PerlPackages.bz2 are stored to /var/cache/apt/apt-pm/.

  clean
    Remove all files fom cache dir.

AUTHOR
    jozef@kutej.net, `<jkutej at cpan.org>'

LICENSE AND COPYRIGHT
    Copyright 2010, Jozef Kutej `<jkutej at cpan.org>'

    This program is free software; you can redistribute it and/or modify it
    under the terms of either: the GNU General Public License as published
    by the Free Software Foundation; or the Artistic License.

    See http://dev.perl.org/licenses/ for more information.

