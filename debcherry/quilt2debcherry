#!/bin/bash

set -e
me=$(basename $0)

DEP3_RE='^ *(Origin|Bug.*|Forwarded|Reviewed-by|Acked-By|Applied-Upstream|Patch-Name):'

if [ $# = 0 ]; then
    if [ -r debian/patches/series ]; then
        files=$(grep -v '^ *#' debian/patches/series | sed 's,^,debian/patches/,')
    else
        echo "No patches specified on no debian/patches/series found, nothing to do?" 1>&2
        exit 1
    fi
else
    files="$*"
fi

function getheader () {
    header=$1
    shift
    while [ $# -gt 0 ]; do
        headername="$1"
        contents=$(echo "$header" | sed -n "s/^ *$headername: *// p")
        if [ -n "$contents" ]; then
            echo "$contents"
            break
        fi
        shift
    done
}

function getdesc () {
    echo "$1" | perl -ne 's/^(\S+): *// and $_ .= "\n" and $1 !~ /^(Subject|Description)$/i and next; s/^ //; print'
}

for patchfile in $files; do
    if [ "$patchfile" = "debian/patches/series" ]; then
        break
    fi
    patchname=$(basename "$patchfile")
    fullheader=$(quilt header "$patchfile")
    githeader=$(echo "$fullheader" | egrep -iv "$DEP3_RE")
    notes=$(printf "%s\nPatch-Name: %s" "$fullheader" "$patchname" | egrep -i "$DEP3_RE")
    echo "Trying 'git am $patchfile'"
    if ! egrep -iv "$DEP3_RE" "$patchfile" | grep -iv "^ *Author:" | git am; then
        echo "'git am $patchfile' failed, constructing commit"
        git am --abort 2>/dev/null || true
        git apply --index "$patchfile"
        message=$(getdesc "$githeader")
        git commit -m "$message"
        date=$(getheader "$githeader" Date Last-Update)
        if [ -n "$date" ]; then
            date=$(date -d "$date" --iso-8601)
            echo "trying to reset date to $date"
            git commit --amend --date "$date" -C HEAD || true
        else
            date=$(git log -1 --format="%aI" -- "$patchfile")
            echo "trying to reset date to $date"
            git commit --amend --date "$date" -C HEAD || true
        fi
        author=$(getheader "$githeader" Author From)
        if [ -n "$author" ]; then
            echo "trying to reset author to $author"
            if ! git commit --amend --author "$author" -C HEAD; then
                notes=$(echo "$notes"; echo "Author: $author")
            fi
        fi
        echo "committed OK"
    else
        echo "git am went OK"
        author=$(getheader "$githeader" Author)
        if [ -n "$author" ]; then
            notes=$(echo "$notes"; echo "Author: $author")
        fi
    fi
    printf "Setting commit notes..."
    git notes add -m "$notes"
    printf "ok, notes now:\n"
    git notes show | sed 's/^/ /'
done
echo "Conversion completed successfully!"
