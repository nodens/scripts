#!/bin/sh
# Script to refresh a debcherry patch
#
# use when run-debcherry produces a debcherry-fixup-patch instead of
# cleanly dropping a hunk of an existing patch, with the original annotated
# patch-commit as first argument and "upstream" as second

if [ $# -lt 2 ]; then
    echo "usage: $0 <commit> upstream"
    exit 1
fi

commit=$(git rev-parse $1)
shortcommit=$(git rev-parse --short $1)

branch=$2


oldbranch=$(git symbolic-ref HEAD)

oldbranch=${oldbranch#refs/heads/}

# get a detached HEAD
hash=$(git rev-parse $branch) || exit 1
if ! git checkout $hash 1>/dev/null 2>&1; then
    echo "checkout $hash failed" 1>&2
    exit 1;
fi


# get a commit containing whatever changes from $commit are not
# already in $branch
if ! git cherry-pick -e $commit ; then
    echo "Starting subshell to resolve conflicts"
    echo "exit to complete complete cherry-pick"
    echo "exit 1 to abort"
    if  ${SHELL-sh} < /dev/tty ; then
        git commit
    else
        git checkout -f ${oldbranch}
        exit 1;
    fi
fi

newcommit=$(git rev-parse HEAD)
message=$(printf "refresh $shortcommit against $branch\n\nGit-Refresh: $commit $newcommit")
git checkout $oldbranch
git merge --no-ff -m "$message" $newcommit

git notes copy $commit $newcommit
