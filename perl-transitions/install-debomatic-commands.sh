#!/bin/sh

# we try to preserve the original order in file mtime stamps
# hence the sleep calls
#
# split(1) is parallelized so no help there

cat "$@" | \
while read command package dist rest; do
    echo "$command $package $dist $rest" > /srv/debomatic/incoming/"$command-$dist-$package".commands
    sleep .01
done
