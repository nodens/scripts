#!/bin/bash

set -e
shopt -s nullglob

ARCH=amd64
VER=5.31

TEMP=$(getopt -o a:d:b: --long arch: --long distribution: -n 'gen-debomatic-all.sh' -- "$@")
eval set -- "$TEMP"

while true; do
  case "$1" in
    -a|--arch) ARCH=$2; shift 2 ;;
    -d|--distribution) DIST=$2; shift 2 ;;
    --) shift; break ;;
    *) echo "Internal error!"; exit 1 ;;
  esac
done

if [ -z "$1" ]; then
    echo "Usage: $0 [ -a ARCH ] [ -d DISTRIBTUION ] perl.sources"
    exit 1
fi

input_file=$(realpath $1)

if [ ! -r "$input_file" ]; then
    echo "$input_file is not a readable file"
    exit 1
fi

if [ -z "$DIST" ]; then
    DIST=perl-$VER-throwaway
fi

grep -v '^#' $input_file | while read srcname_srcvers; do
    echo "rebuild $srcname_srcvers $DIST"
done
