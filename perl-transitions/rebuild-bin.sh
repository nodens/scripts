#!/bin/bash

set -e
shopt -s nullglob

ARCH=amd64
VER=5.22
FULLVER=$VER.0
NMU_MSG="Rebuild for Perl $FULLVER"
WORK_PREFIX=~
REPO_PREFIX=~
START_ROUND=1

TEMP=$(getopt -o a:r:b: --long arch:,round:,basedir: -n 'rebuild-bin.sh' -- "$@")
eval set -- "$TEMP"

while true; do
  case "$1" in
    -a|--arch) ARCH=$2; shift 2 ;;
    -r|--round) START_ROUND=$2; shift 2 ;;
    -b|--basedir) WORK_PREFIX=$2; shift 2 ;;
    --) shift; break ;;
    *) echo "Internal error!"; exit 1 ;;
  esac
done

if [ -z "$1" ]; then
    echo "Usage: $0 [ -a ARCH ] [ -r START_ROUND ] [ -b BASEDIR ] perlapi.out"
    exit 1
fi

input_file=$(realpath $1)

if [ ! -r "$input_file" ]; then
    echo "$input_file is not a readable file"
    exit 1
fi

END_ROUND=$(grep -v '^#' $input_file | sort -n -k2 $input_file | tail -1 | cut -f2 -d\  )
ROUNDS=$(seq $START_ROUND $END_ROUND)
SCHROOT_NAME=perl-$VER-$ARCH-sbuild

mkdir -p $WORK_PREFIX/build $WORK_PREFIX/logs $WORK_PREFIX/build-done

cd $WORK_PREFIX/build

repo_changed=0

sbuild-update -ud $SCHROOT_NAME
for wantround in $ROUNDS; do
    grep -v '^#' $input_file | while read package round srcname_srcvers next_binnmu_number; do
		if [ $round == $wantround ]; then
			sbuild -j 2 -q --arch $ARCH -v -c $SCHROOT_NAME -d unstable --make-binNMU="$NMU_MSG" --binNMU=$next_binnmu_number $srcname_srcvers || true
		fi
	done
    for i in *.changes; do
        reprepro -b $REPO_PREFIX/repo-$FULLVER --ignore=wrongdistribution include perl-$FULLVER $i || true
        repo_changed=1
    done
    if [ $repo_changed == 1 ]; then
        sbuild-update -ud $SCHROOT_NAME
    fi
    for f in *.build; do
        if [ -L $f ]; then
            rm -f $f
        else
            mv $f $WORK_PREFIX/logs
        fi
    done
    for f in *; do
        mv $f $WORK_PREFIX/build-done
    done
done
