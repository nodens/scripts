#!/bin/bash

set -e

ARCHS="i386 amd64"
REPO_PATH=~/repo-5.22.0/
REMOTE_REPO_PATH=people.debian.org:public_html/perl/test/perl-5.22.0
SUCCESS_PATTERN="Status: successful"
UPDATE_INPUT_FILE=1

TEMP=$(getopt -o nt: -n 'rebuild-bin.sh' -- "$@")
eval set -- "$TEMP"

TODAY=$(date -I)

while true; do
  case "$1" in
    -n) UPDATE_INPUT_FILE=0; shift ;;
    -t) TODAY=$2; shift 2 ;;
    --) shift; break ;;
    *) echo "Internal error!"; exit 1 ;;
  esac
done

if [ -z "$1" ]; then
    echo "Usage: $0 [ -n ] <old suffix>"
    echo "-n means use an existing diff file"
    exit 1
fi


YESTERDAY=$1
BASEDIR=~/rebuild-test/$TODAY

if [ $UPDATE_INPUT_FILE == 1 ]; then
    if [ -e "perlapi.out.$TODAY" -o -e "perl.sources.$TODAY" ]; then
        echo "perlapi.out.$TODAY or perl.sources.$TODAY already exists; aborting"
        exit 1
    fi

    sudo apt-get update
    make clean && make perlapi.out perl.sources
    cp perlapi.out perlapi.out.$TODAY
    cp perl.sources perl.sources.$TODAY

    diff -u perlapi.out.$YESTERDAY perlapi.out.$TODAY |grep -v ^++ | grep ^+|sed -e 's/^+//' > perlapi.out.$TODAY.diff
    diff -u perl.sources.$YESTERDAY perl.sources.$TODAY |grep -v ^++ | grep ^+|sed -e 's/^+//' > perl.sources.$TODAY.diff
fi

if [ $(stat -c '%s' perlapi.out.$TODAY.diff) -eq 0 ]; then
    echo "No binary rebuilds to do"
else
    for arch in $ARCHS; do
        ./rebuild-bin.sh -a $arch -b $BASEDIR perlapi.out.$TODAY.diff
    done
    (
        echo "Successful:"
        grep -l "$SUCCESS_PATTERN" $BASEDIR/logs/*.build || true
        echo -e "\nFailed:"
        grep -L "$SUCCESS_PATTERN" $BASEDIR/logs/*.build || true
        echo -e "\nNow run: rsync --delete -ave ssh $REPO_PATH/ $REMOTE_REPO_PATH/"
    ) | mail -s "perl rebuild report from $(hostname --fqdn)" $USER
fi

if [ $(stat -c '%s' perl.sources.$TODAY.diff) -eq 0 ]; then
    echo "No arch-all rebuilds to do"
else
    ./rebuild-all.sh -b $BASEDIR/all perl.sources.$TODAY.diff
    (
        echo "Successful:"
        grep -l "$SUCCESS_PATTERN" $BASEDIR/all/logs/*.build
        echo -e "\nFailed:"
        grep -L "$SUCCESS_PATTERN" $BASEDIR/all/logs/*.build
    ) | mail -s "perl (archall) rebuild report from $(hostname --fqdn)" $USER
fi
