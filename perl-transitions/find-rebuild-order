#!/usr/bin/perl -w
use strict;

use AptPkg::Config '$_config';
use AptPkg::System '$_system';
use AptPkg::Cache;
use AptPkg::Source;
use Getopt::Std;

=pod

=head1 NAME

find-rebuild-order - plan a rebuild for a set of uninstallable packages

=head1 DESCRIPTION

Given a list of uninstallable packages, try to find the correct order
for rebuilding them. This is done by scanning the build-dependencies
and their recursive dependencies for any other uninstallable packages

The 'build-essential' package is always scanned at startup for
uninstallable recursive dependencies. These require manual attention
because nothing can be rebuilt before 'build-essential' can be installed.

Optionally, a list of essential packages (those having the Essential:yes
field) can be read from a separate file and checked for any uninstallable
recursive dependencies. These require manual attention because all
packages may rely upon them being installed at build time.

Note that this program is supposed to be run on an up to date Debian
sid installation, as it uses the local apt cache through the AptPkg::*
modules in libapt-pkg-perl.

=head1 USAGE

B<find-rebuild-order> [ B<-v> ] [ B<-d> ] [ S<B<-e> I<ESSENTIAL-FILE>> ]
S<( B<-f> I<FILE> | I<package1> [ I<...> ] )>

=over

=item -v

Enable verbose output on standard error. You'll probably want this.

=item -d

Enable debugging output on standard error. You probably won't want this.

=item -f FILE

Read the list of packages from file FILE, one package per line. Perl-style
comments (#) are allowed and skipped. Only the first field on each line
is significant.

If '-f' is not specified, the list of packages are expected as
command-line arguments.

=item -e ESSENTIAL-FILE

Read the list of essential packages from file ESSENTIAL-FILE, one package
per line. Perl-style comments (#) are allowed and skipped. Only the
first field on each line is significant.

=back

=head1 OUTPUT FORMAT

The machine-readable output consists of lines of the form 

PACKAGE ROUND SOURCE_VERSION BINNMU_VERSION

where ROUND denotes the rebuild order increasing from 1. It may be 0 for
dependencies of essential or build-essential packages, or 'U' (undefined)
for packages that need each other in a circular manner for building.

The output is always sorted by ROUND, and circular build-dependencies
('U') are detected and printed in the end of the run.

The SOURCE_VERSION field contains the name of the source package and
its version, joined with an underscore (C<_>). This is the format
consumed by sbuild(1).

The BINNMU_VERSION field contains a single number, the next free
binNMU version. For example, if the archive has libfoo-bar-perl version
1.2-2, BINNMU_VERSION is 1. If the version in the archive is 1.2-2+b1,
BINNMU_VERSION will be 2. Again, this is field is meant for sbuild(1)
consumption.

Additionally, explanations targeted at humans (mostly listing reverse
dependencies of the packages) are given on separate lines as Perl-style
comments (ie. starting with the '#' sign.)

=head1 BUGS AND LIMITATIONS

Only the first one of alternative dependencies is scanned, which is intended
to be an approximation of what the Debian buildds do.

Version numbers in versioned dependencies are ignored.

Architecture specifications in dependencies are ignored.

Virtual packages are skipped with a warning.

The program is big, slow and recursive.

=head1 NOTES

This program was written to help with the Debian Perl 5.10 transition,
but it might be generic enough to be useful for other purposes too.

The package dependency information comes from the APT cache via the
AptPkg modules (packaged in Debian as libapt-pkg-perl). Multiple suites
in /etc/apt/sources.list may confuse this program, although it tries
to look up the latest version of each package.

=head1 COPYRIGHT AND LICENSE

Copyright 2008 Niko Tyni <ntyni@debian.org>.

This program is licensed under the terms of the GNU General Public License
(GPL), version 2 or later, as published by the Free Software Foundation.

=cut

(my $self = $0) =~ s#.*/##;

sub usage {
    print STDERR "Usage: $self [-e <essential-file>] [-d] [-v] (-f <file> | package1 [...])\n";
    exit 1;
}

my %opts;
getopts('e:dvf:', \%opts) or usage;

my @packages;
my @essential;

my $debug   = exists $opts{d};
my $verbose = exists $opts{v};

if (exists $opts{f}) {
    @packages = read_packages_from_file($opts{f});
} else {
    @packages = @ARGV;
}

# From /usr/share/doc/libapt-pkg-perl/examples/apt-cache
#
# initialise the global config object with the default values and
# setup the $_system object
$_config->init;
$_system = $_config->system;
my $versioning = $_system->versioning;
# supress cache building messages
$_config->{quiet} = 2;


my $bincache = AptPkg::Cache->new;
my $srccache = AptPkg::Source->new;

# a mapping from binary package name to an AptPkg::Source object
my %source_of;

# the main list of uninstallable packages we're working with
# the values are AptPkg::Cache::Version objects for the latest
# binary package
my %uninstallable;

# a global cache of uninstallable recursive dependencies of a package,
# so we don't need to hunt them down each time
my %uninstallable_dependencies_cache;

my %binnmu_vers;
my %src_with_vers;

notice("reading apt cache");

for my $pkg (@packages) {
    my @slist = $srccache->find($pkg) or do {
        warn("no such source package: $pkg");
        next;
    };
    my $s = src_latest(@slist);
    $src_with_vers{$pkg} = $s->{Package} . "_" . $s->{Version};

    my $blist = $bincache->{$pkg} or do {
        warn("no such binary package: $pkg");
        next;
    };

    my $b = bin_latest($blist);
    debug("looked up $pkg/$b->{VerStr} from the package cache");
    $binnmu_vers{$pkg} = ($b->{VerStr} =~ /\+b(\d+)$/ ? 1+$1 : 1);

    $source_of{$pkg} = $s;
    $uninstallable{$pkg} = $b;
}

notice("starting with " . (scalar keys %uninstallable) . " uninstallable packages");
print "# Generated by $self\n# <package> <rebuild round> <srcname_srcvers> <next_binnmu_number>\n";

if (exists $opts{e}) {
    notice("scanning file $opts{e} for essential packages");
    @essential = read_packages_from_file($opts{e});
    notice("check if any dependencies of essential packages are uninstallable");
    my $found = 0;
    for my $e (@essential) {
        my $uninstallable_essential = find_uninstallable_dependencies($e, {}, []);
        for (keys %$uninstallable_essential) {
            print "# $_ is virtually essential (" 
                  . join(" -> ", @{$uninstallable_essential->{$_}}, $_)
                  . ")\n";
            print "$_ 0 $src_with_vers{$_} $binnmu_vers{$_}\n";
            $found++;
            delete $uninstallable{$_};
            # reset the cache, it may be invalid after the delete
            %uninstallable_dependencies_cache = ();
        }
    }    
    notice("found $found virtually essential packages");
}

{ # smaller scope for the variables
    notice("check if any dependencies of build-essential packages are uninstallable");
    my $found = 0;
    my $uninstallable_build_essential = find_uninstallable_dependencies('build-essential', {}, []);
    for my $be (keys %$uninstallable_build_essential) {
        print "# $be is virtually build-essential ("
              . join(" -> ", @{$uninstallable_build_essential->{$be}}, $be)
              . ")\n";
        print "$be 0\n";
        delete $uninstallable{$be};
        $found++;
        # reset the cache, it may be invalid after the delete
        %uninstallable_dependencies_cache = ();
    }
    notice("found $found virtually build-essential packages");
}

my $round = 1;

while (1) { # actually, as long as we find something to build
  my %needed_by;
  my %needs_packages;

  my $count = scalar keys %uninstallable;
  notice("starting round $round: $count uninstallable packages left");

  while (my ($name, $b) = each %uninstallable) {
    my $s = $source_of{$name};

    # only use the first alternative in OR'd dependencies, skip others
    my $skip_next = 0;
    for (@{$s->{BuildDepends}{"Build-Depends"}}) {
        my $skip_this = $skip_next;
        if (defined $_->[1] && ($_->[1] == AptPkg::Dep::Or)) {
            $skip_next = 1;
        } else {
            $skip_next = 0;
        }
        next if $skip_this;
        my $visited = {};
        debug("$s->{Package}: Build-Depends on $_->[0]");
        $uninstallable_dependencies_cache{$_->[0]} =
            find_uninstallable_dependencies($_->[0], $visited, []);
        my @result = keys %{$uninstallable_dependencies_cache{$_->[0]}};
        debug("$s->{Package} => $_->[0] ->" . join(" ", @result))  if @result;
        for (@result) {
            $needed_by{$_}{$name} = 1;
            $needs_packages{$name}{$_} = 1;
        }
    }
  }

  # output all buildable packages found

  my $buildable_found = 0;
  for (sort keys %uninstallable) {
    if (!exists $needs_packages{$_}) {
        $buildable_found = 1;
        delete $uninstallable{$_};
        # include any reverse dependencies as a comment
        if (exists $needed_by{$_}) {
            print "# $_ is needed by " 
                  . join(" ", sort keys %{$needed_by{$_}})
                  . "\n";
        }
        print "$_ $round $src_with_vers{$_} $binnmu_vers{$_}\n";
    }
  }

  if (!$buildable_found) {
    my $left = scalar keys %uninstallable;
    if ($left) {
        notice("circular dependencies found, quitting with $count uninstallable packages left");
        for (sort keys %uninstallable) {
            print "# $_ circular dependency: needs "
            . join(" ", sort keys %{$needs_packages{$_}})
            . "\n";
            if (exists $needed_by{$_}) {
                print "# $_ circular dependency: needed by "
                . join(" ", sort keys %{$needed_by{$_}})
                . "\n";
            }
            print "$_ U\n";
        }
    }
    last; # this is the only exit place: no buildable packages found anymore
  }

  # reset the cache on each round
  %uninstallable_dependencies_cache = ();

  $round++;
}

notice("all done!");

# find uninstallable recursive dependencies of a given package
#
# the second argument is a hash of all visited packages to break
# circular dependencies
#
# the third argument is the dependency path; it's stored in the result hash
# for informational purposes only

sub find_uninstallable_dependencies {
    my ($package, $visited, $path) = @_;
    my $level = scalar @$path;
    my $prefix = " "x$level;
    my %ret;
    $visited->{$package} = 1;
    if (exists $uninstallable{$package}) {
        $ret{$package} = $path;
    }
    if (exists $uninstallable_dependencies_cache{$package}) {
        debug("${prefix}$package dependencies already known, returning");
        return $uninstallable_dependencies_cache{$package};
    }
    my @dependencies = lookup_dependencies($package);
    debug("${prefix}$package depends on " . 
        ((scalar @dependencies) ? join(",", @dependencies) : "nothing"));
    for my $dep (@dependencies) {
        if (exists $visited->{$dep}) {
            #debug("${prefix}already visited $dep, skipping");
            next;
        }
        %ret = (%ret, %{find_uninstallable_dependencies($dep, $visited, [ @$path, $package ])});
    }
    return \%ret;
}

# just find recursive dependencies of a given package
sub lookup_dependencies {
    my $pkg = shift;
    my $blist = $bincache->{$pkg};
    if (!$blist) {
        debug("$pkg not found, skipping");
        return ();
    } elsif (!$blist->{VersionList}) {
        if (!$blist->{ProvidesList}) {
            debug("$pkg is not a real versioned package and doesn't provide anything, skipping");
            return ();
        }
        return lookup_dependencies($blist->{ProvidesList}[0]{OwnerPkg}{Name});
    };

    my $b = bin_latest($blist);
    my $depends = $b->{DependsList};
    my $skipnext = 0;
    my @ret;

    for (@$depends) {
        next if $_->{DepType} ne "Depends" && $_->{DepType} ne "PreDepends";
        my $skipthis = $skipnext;
        if (defined $_->{CompType} && ($_->{CompType} & AptPkg::Dep::Or)) {
            $skipnext = 1;
        } else {
            $skipnext = 0;
        }
        next if $skipthis;
        my $name = $_->{TargetPkg}->{Name};
        $name =~ s/:.*//; # foo:any -> foo
        push @ret, $name;
    }
    return @ret;
}

sub read_packages_from_file {
    my $file = shift;
    my @ret;

    debug("reading $file");
    if ($file ne "-") {
        open(IN, '<', $file) or die("open $file for reading: $!");
    } else {
        *IN = *STDIN;
    }
    while (<IN>) {
        next if /^\s*(#|\n)/;
        chomp;
        my ($package, @rest) = split;
        push @ret, $package;
    }
    close IN;
    return @ret;
}

sub bin_latest {
    my $p = shift;
    return (sort bin_byversion @{$p->{VersionList}})[-1];
}

sub bin_byversion {
    return $versioning->compare($a->{VerStr}, $b->{VerStr});
}

sub src_latest {
    return (sort src_byversion @_)[-1];
}

sub src_byversion {
    return $versioning->compare($a->{Version}, $b->{Version});
}

sub debug {
    print STDERR "DEBUG: " . (shift) . "\n" if $debug;
}

sub notice {
    print STDERR "NOTE: " . (shift) . "\n" if $verbose;
}
