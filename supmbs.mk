#!/usr/bin/make -f
# This template is for building normal perl packages, meant to be included from
# debian/rules. It only supports single-binary packages, or multi-binary
# packages that only have one phase of building. Installs to the first binary
# named.
#
# It was created by Martín Ferrari <martin.ferrari@gmail.com>, based on work
# from Marc Brockschmidt <marc@dch-faq.de> and ideas (and variable names) from
# cdbs, for the Debian Perl Group (http://pkg-perl.alioth.debian.org/) but may
# be used freely wherever it is useful.
# Variables

DEB_SOURCE_PACKAGE := $(shell dpkg-parsechangelog | sed -n 's/Source: //p;T;q')
DEB_VERSION := $(shell dpkg-parsechangelog | sed -n 's/Version: //p;T;q')

export DH_VERBOSE
export PERL_MM_USE_DEFAULT

# If set to a true value then MakeMaker's prompt function will
# always return the default without waiting for user input.
PERL_MM_USE_DEFAULT ?= 1
PERL ?= /usr/bin/perl

# Allow disabling build optimation by setting noopt in
# $DEB_BUILD_OPTIONS
CFLAGS = -Wall -g
ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
    CFLAGS += -O0
else
    CFLAGS += -O2
endif

# Verbatim from cdbs:
# This odd piece of coding is necessary to hide the warning message
# "I have no package to build" while preserving genuine error messages.
DEB_INDEP_PACKAGES := $(strip \
        $(shell dh_listpackages -i 2>/dev/null || dh_listpackages -i))
DEB_ARCH_PACKAGES := $(strip \
        $(shell dh_listpackages -a 2>/dev/null || dh_listpackages -a))
DEB_ALL_PACKAGES := $(DEB_INDEP_PACKAGES) $(DEB_ARCH_PACKAGES)

# If it has both kind of binaries, is none
ARCH_PACKAGE := $(if $(DEB_INDEP_PACKAGES),1,)
INDEP_PACKAGE := $(if $(DEB_ARCH_PACKAGES),1,)

PACKAGE ?= $(word 1, $(shell dh_listpackages))
DESTDIR ?= $(CURDIR)/debian/$(PACKAGE)

# Find out if we need to call a patch system
PATCH_SYS ?= quilt
ifeq ($(strip $(PATCH_SYS)),quilt)
    PATCH_SYS_INCLUDE := /usr/share/quilt/quilt.make
    PATCH_TARGET = $(QUILT_STAMPFN)
    UNPATCH_TARGET := unpatch
else ifeq ($(strip $(PATCH_SYS)),dpatch)
    PATCH_SYS_INCLUDE := /usr/share/dpatch/dpatch.make
    PATCH_TARGET = $(DPATCH_STAMPFN)
    UNPATCH_TARGET := unpatch
else
    $(error Patch system $(PATCH_SYS) is unknown)
endif

ifneq ($(strip $(wildcard debian/patches)),)
    DO_PATCH := yes
    include $(PATCH_SYS_INCLUDE)
else
    DO_PATCH :=
    PATCH_TARGET :=
    UNPATCH_TARGET :=
endif

ifneq ($(strip $(wildcard Build.PL)),)
    MOD_TYPE := Build.PL
else ifneq ($(strip $(wildcard Makefile.PL)),)
    MOD_TYPE := Makefile.PL
else
    $(error What kind of module is this?)
endif

Makefile: $(PATCH_TARGET) $(MOD_TYPE)
	[ "$(MOD_TYPE)" != Build.PL ] || \
	    $(PERL) Build.PL installdirs=vendor
	[ "$(MOD_TYPE)" != Makefile.PL ] || \
	    $(PERL) Makefile.PL INSTALLDIRS=vendor

build: build-stamp
build-stamp: Makefile
	dh_testdir
	# Doesn't hurt to use CFLAGS
	[ "$(MOD_TYPE)" != Build.PL ] || \
	    OPTIMIZE="$(CFLAGS)" $(PERL) Build \
	    $(PERL) Build test
	[ "$(MOD_TYPE)" != Makefile.PL ] || \
	    $(MAKE) OPTIMIZE="$(CFLAGS)" LD_RUN_PATH="" \
	    $(MAKE) \
	    $(MAKE) test
	touch $@

clean: $(UNPATCH_TARGET)
	dh_testdir
	dh_testroot # Does this still have sense?
	dh_clean build-stamp install-stamp
	[ ! -f Build ] || $(PERL) Build distclean
	[ ! -f Makefile ] || $(MAKE) realclean

install: install-stamp
install-stamp: build-stamp
	dh_testdir
	dh_testroot
	dh_clean -k
	dh_installdirs
	[ "$(MOD_TYPE)" != Build.PL ] || \
	    $(PERL) Build install destdir=$(DESTDIR) create_packlist=0
	[ "$(MOD_TYPE)" != Makefile.PL ] || \
	    $(MAKE) install DESTDIR=$(DESTDIR) PREFIX=/usr
	[ -z "$(INDEP_PACKAGE)" || ! -d $(DESTDIR)/usr/lib/perl5 ] || \
	    rmdir --ignore-fail-on-non-empty --parents --verbose \
	    $(DESTDIR)/usr/lib/perl5
	[ -z "$(ARCH_PACKAGE)" || ! -d $(DESTDIR)/usr/share/perl5 ] || \
	    rmdir --ignore-fail-on-non-empty --parents --verbose \
	    $(DESTDIR)/usr/share/perl5
	touch $@

binary-arch:
	# We have nothing to do here for an architecture-independent package

binary-indep: build install
	dh_testdir
	dh_testroot
	dh_installexamples
	dh_installdocs
	dh_installchangelogs
	dh_installman
	dh_installinfo
	dh_install
	dh_link
	[ -n "$(ARCH_PACKAGE)" ] || dh_shlibdeps
	[ -n "$(ARCH_PACKAGE)" ] || dh_strip
	dh_perl
	dh_compress
	dh_fixperms
	dh_installdeb
	dh_gencontrol
	dh_md5sums
	dh_builddeb

source diff:
	@echo >&2 'source and diff are obsolete - use dpkg-source -b'; false

binary: binary-indep binary-arch
.PHONY: build install clean binary-indep binary-arch binary

