#
# this Makefile is used to generate http://pkg-perl.alioth.debian.org/patchedit/
#

all:
	git svn rebase
	find -name series | sort | perl -MIO::Any -MFile::Basename=dirname -ne 'chomp; my $$s = $$_; my $$bd=dirname($$s); $$bd=~s{^\./}{}; my @pl=map { $$bd."/".$$_ } grep { $$_ !~ m/^\s*$$/ } map { chomp; $$_ } grep { not m/^#/ } split("\n", IO::Any->slurp($$s)); print map { $$_."\n" } @pl;' | xargs prove --exec "patchedit check -o" -m -Q --formatter=TAP::Formatter::HTML -P HTML=outfile:output.html.new,force_inline_css:0 || /bin/true
	perl -lane 's{file:/usr/local/share/perl/.+?/TAP/Formatter/HTML/}{}g; print $$_' -i output.html.new
	mv output.html.new output.html

