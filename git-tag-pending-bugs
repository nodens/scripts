#!/usr/bin/perl -w

# Find bugs closed by this commit and tag them pending in the BTS
#
# Synopsis: $0 repos_path revision
#
# Copyright © 2008, 2011, 2014 Damyan Ivanov <dmn@debian.org>
#
# This program is free software. You may distribute it under
# the terms of Perl

use strict;
use Cwd qw(realpath);
use File::Basename qw(basename);
use Getopt::Long;
use Git;
use Mail::Send;
use Sys::Hostname::Long;
use Text::Wrap qw(wrap);

use constant FROM	=> 'pkg-perl-maintainers@lists.alioth.debian.org';

our $opt_debug;

GetOptions( 'debug!' => \$opt_debug )
    or exit 1;

my( $repo_path, $revision ) = @ARGV;

my $git = Git->repository($repo_path);
my ( $package, $author, $log );

$package = basename(realpath($repo_path));
$package =~ s/\.git$//;

my $has_debian_changes;
my $state = 'filling-log';

for my $line ( $git->command( show => '--format=medium' => $revision ) ) {
    if ( $state eq 'filling-log' ) {
        if ( defined($log) ) {
            $state = 'diff-seen', next if $line =~ /^diff/;
            $log .= "\n" if $log ne '';
            $log .= $line;
            next;
        }

        $log = '', next if $line eq '';

        $author = $1 if $line =~ /^Author: (.+) </;
    }
    elsif ( $state eq 'diff-seen' ) {
        $has_debian_changes = 1, last if $line =~ m{^(?:\+\+\+|---) (?:a|b)/debian/};
    }
    else {
        die "Should not happen [$state]";
    }
}

exit unless $has_debian_changes;

my $branch = $git->command_oneline( branch => '--contains', $revision );

$branch =~ s/^\* //;

my @bugs;
my $log_copy = $log;
while( $log_copy =~ s/(closes:\s*(?:bug)?\#\s*\d+(?:,\s*(?:bug)?\#\s*\d+)*)//is )
{
  my $match = $1;
  push @bugs, $1 while $match =~ s/#(\d+)//;
}

exit unless @bugs;

my $msg = Mail::Send->new(
  Subject=>"Pending fixes for bugs in the $package package",
  Bcc	=> 'control@bugs.debian.org',
);
$msg->add('From', FROM);
$msg->add('X-Mailer', (basename $0).' running on '.hostname_long);
$msg->add('X-Debbugs-No-Ack', 'please');
$msg->add('X-Git-Repository', $repo_path);
$msg->add('X-Debian', (basename $0));
$msg->add('X-Debian-Package', $package);
$msg->add('MIME-Version', '1.0');
$msg->add('Content-Type', 'text/plain; charset=utf-8');     
$msg->add('Content-Transfer-Encoding', '8bit');

$msg->to(
    join(
        ', ',
        map(
            ("$_\@bugs.debian.org", "$_-submitter\@bugs.debian.org"),
            @bugs )
    )
);

my $out;

if ( $opt_debug ) {
    $out = $msg->open('testfile');
}
else {
    $out = $msg->open();
}
$Text::Wrap::columns = 70;
$Text::Wrap::huge = 'overflow';

$out->print("tag $_ + pending\n") foreach @bugs;
$out->print("thanks\n\n");
$out->print(
    wrap(
        '',
        '',
        "Some bugs in the $package package are closed "
            . "in revision $revision in branch '$branch' by $author"
    ),
    "\n\n"
);
$out->print(
    wrap(
        '',
        '',
        "The full diff can be seen at https://anonscm.debian.org/cgit/pkg-perl/packages/$package.git/commit/?id="
            . substr( $revision, 0, 7 )
    ),
    "\n\n"
);
$out->print("Commit message:\n\n$log\n");
$out->close;
